#!/bin/sh

PKG="mod_intercept_form_submit"

die () {
    echo "$1" >&2
    exit 1
}

# If source not found, download it with dnf
if [ ! -d ./source ]; then
    # Extract source from srpm
    dnf download --source ${PKG} && \
    rpm2cpio ${PKG}*.rpm|cpio -id && \
    mkdir source && \
    tar -zxf ${PKG}-*.tar.gz -C source --strip-components=1
    if [ $? != 0 ]; then
        echo "Failed to download upstream tests"
        exit 1
    fi
fi

pushd ./source || die "missing source directory"
rm -f ${PKG} tests/*.log 2>/dev/null
# ln -s /usr/bin/${PKG} || die "failed to link ${PKG} binary"

FAIL=0

# run the tests
#
tests/config.sh
setsebool allow_httpd_mod_auth_pam 1
systemctl start httpd
tail -f /var/log/httpd/*_log &
tests/run.sh
kill %1
